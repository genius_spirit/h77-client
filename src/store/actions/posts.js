import axios from '../../axios-api';
import {CATCH_ERROR, FETCH_POSTS_SUCCESS} from "./actionTypes";

export const fetchPostsSuccess = posts => {
  return {type: FETCH_POSTS_SUCCESS, posts};
};

export const fetchPosts = () => {
  return dispatch => {
    axios.get('/posts').then(
      response => dispatch(fetchPostsSuccess(response.data))
    )
  }
};

export const createPost = post => {
  return dispatch => {
    return axios.post('/posts', post).then(() => {}, error => dispatch(catchError(JSON.stringify(error.response.data))));
  };
};

export const catchError = error => {
  return { type: CATCH_ERROR, error };
};