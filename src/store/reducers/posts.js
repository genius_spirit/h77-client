import {CATCH_ERROR, FETCH_POSTS_SUCCESS} from "../actions/actionTypes";

const initialstate = {
  posts: [],
  error: ''
};

const reducer = (state = initialstate, action) => {
  switch (action.type) {
    case FETCH_POSTS_SUCCESS:
      return {...state, posts: action.posts};
    case CATCH_ERROR:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;