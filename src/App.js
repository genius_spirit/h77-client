import React, {Component, Fragment} from 'react';
import Toolbar from "./components/Toolbar/Toolbar";
import Posts from "./containers/Posts/Posts";
import AddPostForm from "./containers/AddPostForm/AddPostForm";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header><Toolbar/></header>
        <main className="container">
          <Posts/>
        </main>
        <footer className="container"><AddPostForm/></footer>
      </Fragment>
    );
  }
}

export default App;
