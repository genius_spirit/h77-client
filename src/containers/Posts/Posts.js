import React, {Component, Fragment} from 'react';
import { Image, Panel} from "react-bootstrap";
import {connect} from "react-redux";
import {fetchPosts} from "../../store/actions/posts";

class Posts extends Component {

  componentDidMount() {
    this.props.onFetchPosts();
  }

  render() {
    return(
      <Fragment>
        {this.props.posts.map(post => (
          <Panel key={post.id}>
            <Panel.Heading>{post.author + ', ' + post.datetime}</Panel.Heading>
            <Panel.Body>
              {post.image !== 'undefined' ?
              <Image style={{width: '100px', marginRight: '20px', marginBottom: '10px', cssFloat: 'left'}}
                     src={'http://localhost:8000/uploads/' + post.image}
                     rounded /> : null}
              <Panel.Body>{post.message}</Panel.Body>
            </Panel.Body>
          </Panel>
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchPosts: () => dispatch(fetchPosts())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);