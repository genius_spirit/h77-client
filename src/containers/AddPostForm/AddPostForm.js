import React, {Component, Fragment} from 'react';
import {
  Button,
  Col,
  Collapse,
  ControlLabel,
  Form,
  FormControl,
  FormGroup,
  HelpBlock,
  PageHeader
} from "react-bootstrap";
import {createPost, fetchPosts} from "../../store/actions/posts";
import {connect} from "react-redux";

class AddPostForm extends Component {
  state = {
    post: {
      author: '',
      message: '',
      image: ''
    },
    open: false
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state.post).forEach(key => {
      formData.append(key, this.state.post[key]);
    });

    this.props.onSubmitForm(formData).then(() => {
      this.props.onFetchPosts();
    });
  };

  inputChangeHandler = event => {
    this.setState({
      post: {...this.state.post, [event.target.name]: event.target.value}
    });
  };

  fileChangeHandler = event => {
    this.setState({
      post: {...this.state.post, [event.target.name]: event.target.files[0]}
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader><small><Button bsStyle="success" onClick={() => {this.setState({open: !this.state.open})}}>Add new post</Button></small></PageHeader>
        <Collapse in={this.state.open}>
          <Form horizontal onSubmit={this.submitFormHandler}>

          <FormGroup controlId="postAuthor">
            <Col componentClass={ControlLabel} sm={2}>Author</Col>
            <Col sm={9}>
              <FormControl
                type="text"
                placeholder="Enter Author's name"
                name="author"
                value={this.state.post.author}
                onChange={this.inputChangeHandler}
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="postMessage" validationState={this.props.error ? "error" : null}>
            <Col componentClass={ControlLabel} sm={2}>Description</Col>
            <Col sm={9}>
              <FormControl
                // required
                componentClass="textarea"
                placeholder="Enter your message"
                name="message"
                value={this.state.post.message}
                onChange={this.inputChangeHandler}
              />
              <HelpBlock>{this.props.error ? JSON.parse(this.props.error).error : null}</HelpBlock>
            </Col>
          </FormGroup>

          <FormGroup controlId="productImage">
            <Col componentClass={ControlLabel} sm={2}>Image</Col>
            <Col sm={9}>
              <FormControl
                type="file"
                name="image"
                onChange={this.fileChangeHandler}
              />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">Post</Button>
            </Col>
          </FormGroup>
        </Form>
        </Collapse>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.error
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onSubmitForm: post => dispatch(createPost(post)),
    onFetchPosts: () => dispatch(fetchPosts())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPostForm);
