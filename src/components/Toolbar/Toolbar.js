import React from 'react';
import {Navbar} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Toolbar = () => {
  return(
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <LinkContainer to="/"><a>Posts</a></LinkContainer>
        </Navbar.Brand>
      </Navbar.Header>
    </Navbar>
  )
};

export default Toolbar;